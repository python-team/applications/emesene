This package was debianized by Emilio Pozuelo Monfort <pochu@ubuntu.com> on
Thu, 11 Oct 2007 14:10:47 +0200.

It was downloaded from http://www.emesene.org/

Upstream Authors:

    Riccardo (C10uD) <c10ud.dev@gmail.com>
    Luis Mariano Guerra <luismarianoguerra@gmail.com>

Copyright:

    Copyright © 2009-2010, Riccardo (C10uD) <c10ud.dev@gmail.com>
    Copyright © 2010, Luis Mariano Guerra <luismarianoguerra@gmail.com>
    Copyright © 2008, J. Alexander Treuman <jat@spatialrift.net>
    Copyright © 2005, James Bunton <james@delx.cjb.net>
    Copyright © 2011, Andrea Stagi <stagi.andrea@gmail.com>
    Copyright © 2011, Canonical

License:

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package.  If not, see <http://www.gnu.org/licenses/>.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-3'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For desktop.py, the following terms apply:

    Simple desktop integration for Python. This module provides desktop
    environment detection and resource opening support for a selection
    of common and standardised desktop environments.

    Copyright © 2005-2009, Paul Boddie <paul@boddie.org.uk>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 3 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

On Debian systems, the complete text of the GNU Lesser General
Public License can be found in `/usr/share/common-licenses/LGPL-3'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

themes/sounds/default/* and themes/emotes/default/* are copied from
Pidgin <http://pidgin.im/>

They and are released under the following license:

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this package; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

po/* are released under the same license as emesene (GPL3+) and the
copyright owners are their respective translators.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package.  If not, see <http://www.gnu.org/licenses/>.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-3'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/gui/gtkui/enchant_dicts.py, the following terms apply:

    Copyright © 2004-2008, Ryan Kelly

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.

On Debian systems, the complete text of the GNU Lesser General
Public License can be found in `/usr/share/common-licenses/LGPL-2.1'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/common/NetworkManagerHelper.py, the following terms
apply:

    Copyright © 2010 Mohamed Amine IL Idrissi
    Copyright © 2011 Canonical

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this package; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/papylib/PapyConference.py, the following terms
apply:

    Copyright © 2009 Collabora Ltd

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this package; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/synch/*, the following terms apply:

    Copyright © 2011 Andrea Stagi <stagi.andrea(at)gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this package; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-2'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/papylib/*, the following terms apply:

    Copyright © 2005-2007, Ali Sabil
    Copyright © 2007-2008, Johann Prieur
    Copyright © 2008, Richard Spiers
    Copyright © 2005-2007, Ole André Vadla Ravnås
    Copyright © 2008, Alen Bou-Haidar
    Copyright © 2007, Youness Alaoui
    Copyright © 2009-2010, Collabora Ltd.

    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This package is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA

On Debian systems, the complete text of the GNU General
Public License version 2 can be found in `/usr/share/common-licenses/GPL-2'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For pyiso8601 (used by papylib) the following terms apply:

    Copyright © 2007, Michael Twomey

    Permission is hereby granted, free of charge, to any person obtaining a
    copy of this software and associated documentation files (the
    "Software"), to deal in the Software without restriction, including
    without limitation the rights to use, copy, modify, merge, publish,
    distribute, sublicense, and/or sell copies of the Software, and to
    permit persons to whom the Software is furnished to do so, subject to
    the following conditions:
    
    The above copyright notice and this permission notice shall be included
    in all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
    IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
    CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
    TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
    SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/jabber/pyfacebook/*, the following terms apply:
  
    Copyright © 2008, Samuel Cormier-Iijima

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the author nor the names of its contributors may
      be used to endorse or promote products derived from this software
      without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS``AS IS'' AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/jabber/Worker.py, the following terms apply:

    Copyright © 2009-2010, Emesene

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this package.  If not, see <http://www.gnu.org/licenses/>.

On Debian systems, the complete text of the GNU General
Public License can be found in `/usr/share/common-licenses/GPL-3'.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For pyDes (used by papylib) the following terms apply:

    Released into the Public Domain by Todd Whiteman
    <twhiteman@users.sourceforge.net>.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Debian packaging is Copyright © 2007-2009, Emilio Pozuelo Monfort
 <pochu@ubuntu.com> and is licensed under the GPL-3, see above.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For SleekXMPP the following terms apply:

	Copyright © 2010-2012, Nathanael C. Fritz
	Copyright © 2010-2012, Lance J.T. Stout
	Copyright © 2011, Dalek
	Copyright © 2010, Erik Reuterborg Larsson
	Copyright © 2011, Dann Martens

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/xmpp/SleekXMPP/sleekxmpp/thirdparty/mini_dateutil.py the
following terms apply:

	Copyright © 2003-2011, Gustavo Niemeyer <gustavo@niemeyer.net>
	Copyright © 2008, Red Innovation Ltd., Finland

	Redistribution and use in source and binary forms, with or without
	modification, are permitted provided that the following conditions are met:
	
	    * Redistributions of source code must retain the above copyright notice,
	      this list of conditions and the following disclaimer.
	    * Redistributions in binary form must reproduce the above copyright notice,
	      this list of conditions and the following disclaimer in the documentation
	      and/or other materials provided with the distribution.
	    * Neither the name of the copyright holder nor the names of its
	      contributors may be used to endorse or promote products derived from
	      this software without specific prior written permission.
	
	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
	"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
	LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
	A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
	CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
	EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
	PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
	PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
	LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
	NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/xmpp/SleekXMPP/sleekxmpp/thirdparty/gnupg.py the
following terms apply:

	Copyright © 2008-2012, Vinay Sajip

	===================================================================
	Distribute and use freely; there are no restrictions on further
	dissemination and usage except those imposed by the laws of your
	country of residence.  This software is provided "as is" without
	warranty of fitness for use or suitability for any purpose, express
	or implied. Use at your own risk or not at all.
	===================================================================

	Incorporating the code into commercial products is permitted; you do
	not have to make source available or contribute your changes back
	(though that would be nice).

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/xmpp/SleekXMPP/sleekxmpp/thirdparty/suelta/* the following
terms apply:

	Copyright © 2007-2010, David Alan Cridland

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/e3/xmpp/SleekXMPP/sleekxmpp/thirdparty/ordereddict.py the following
terms apply:

	Copyright © 2009 Raymond Hettinger

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For emesene/themes/conversations/renkoo.AdiumMessageStyle/Contents/Resources/* the following terms apply:

    Copyright © Renkoo, Inc.

    The images, css and html is dual licensed under the BSD and AFL license. 
    The source files for the bubbles can be found at http://www.itorrey.com/adiumx/


    Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer 
    in the documentation and/or other materials provided with the distribution.
    Neither the name of the Torrey Rice and Renkoo nor the names of its contributors may be used to endorse or promote products 
    derived from this software without specific prior written permission.
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT 
    NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
    THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
    HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
    ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Renkoo is a service mark of Renkoo, Inc.

http://www.opensource.org/licenses/bsd-license.php


    This Academic Free License (the "License") applies to any original
    work of authorship (the "Original Work") whose owner (the "Licensor")
    has placed the following notice immediately following the copyright
    notice for the Original Work:

    Licensed under the Academic Free License version 2.1

    1) Grant of Copyright License. Licensor hereby grants You a
    world-wide, royalty-free, non-exclusive, perpetual, sublicenseable
    license to do the following:

    a) to reproduce the Original Work in copies;

    b) to prepare derivative works ("Derivative Works") based upon the Original Work;

    c) to distribute copies of the Original Work and Derivative Works to the public;

    d) to perform the Original Work publicly; and

    e) to display the Original Work publicly.

    2) Grant of Patent License. Licensor hereby grants You a world-wide,
    royalty-free, non-exclusive, perpetual, sublicenseable license, under
    patent claims owned or controlled by the Licensor that are embodied in
    the Original Work as furnished by the Licensor, to make, use, sell and
    offer for sale the Original Work and Derivative Works.

    3) Grant of Source Code License. The term "Source Code" means the
    preferred form of the Original Work for making modifications to it and
    all available documentation describing how to modify the Original
    Work. Licensor hereby agrees to provide a machine-readable copy of the
    Source Code of the Original Work along with each copy of the Original
    Work that Licensor distributes. Licensor reserves the right to satisfy
    this obligation by placing a machine-readable copy of the Source Code
    in an information repository reasonably calculated to permit
    inexpensive and convenient access by You for as long as Licensor
    continues to distribute the Original Work, and by publishing the
    address of that information repository in a notice immediately
    following the copyright notice that applies to the Original Work.

    4) Exclusions From License Grant. Neither the names of Licensor, nor
    the names of any contributors to the Original Work, nor any of their
    trademarks or service marks, may be used to endorse or promote
    products derived from this Original Work without express prior written
    permission of the Licensor. Nothing in this License shall be deemed to
    grant any rights to trademarks, copyrights, patents, trade secrets or
    any other intellectual property of Licensor except as expressly stated
    herein. No patent license is granted to make, use, sell or offer to
    sell embodiments of any patent claims other than the licensed claims
    defined in Section 2. No right is granted to the trademarks of
    Licensor even if such marks are included in the Original Work. Nothing
    in this License shall be interpreted to prohibit Licensor from
    licensing under different terms from this License any Original Work
    that Licensor otherwise would have a right to license.

    5) This section intentionally omitted.

    6) Attribution Rights. You must retain, in the Source Code of any
    Derivative Works that You create, all copyright, patent or trademark
    notices from the Source Code of the Original Work, as well as any
    notices of licensing and any descriptive text identified therein as an
    "Attribution Notice." You must cause the Source Code for any
    Derivative Works that You create to carry a prominent Attribution
    Notice reasonably calculated to inform recipients that You have
    modified the Original Work.
    
    7) Warranty of Provenance and Disclaimer of Warranty. Licensor
    warrants that the copyright in and to the Original Work and the patent
    rights granted herein by Licensor are owned by the Licensor or are
    sublicensed to You under the terms of this License with the permission
    of the contributor(s) of those copyrights and patent rights. Except as
    expressly stated in the immediately proceeding sentence, the Original
    Work is provided under this License on an "AS IS" BASIS and WITHOUT
    WARRANTY, either express or implied, including, without limitation,
    the warranties of NON-INFRINGEMENT, MERCHANTABILITY or FITNESS FOR A
    PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY OF THE ORIGINAL
    WORK IS WITH YOU. This DISCLAIMER OF WARRANTY constitutes an essential
    part of this License. No license to Original Work is granted hereunder
    except under this disclaimer.

    8) Limitation of Liability. Under no circumstances and under no legal
    theory, whether in tort (including negligence), contract, or
    otherwise, shall the Licensor be liable to any person for any direct,
    indirect, special, incidental, or consequential damages of any
    character arising as a result of this License or the use of the
    Original Work including, without limitation, damages for loss of
    goodwill, work stoppage, computer failure or malfunction, or any and
    all other commercial damages or losses. This limitation of liability
    shall not apply to liability for death or personal injury resulting
    from Licensor's negligence to the extent applicable law prohibits such
    limitation. Some jurisdictions do not allow the exclusion or
    limitation of incidental or consequential damages, so this exclusion
    and limitation may not apply to You.

    9) Acceptance and Termination. If You distribute copies of the
    Original Work or a Derivative Work, You must make a reasonable effort
    under the circumstances to obtain the express assent of recipients to
    the terms of this License. Nothing else but this License (or another
    written agreement between Licensor and You) grants You permission to
    create Derivative Works based upon the Original Work or to exercise
    any of the rights granted in Section 1 herein, and any attempt to do
    so except under the terms of this License (or another written
    agreement between Licensor and You) is expressly prohibited by
    U.S. copyright law, the equivalent laws of other countries, and by
    international treaty. Therefore, by exercising any of the rights
    granted to You in Section 1 herein, You indicate Your acceptance of
    this License and all of its terms and conditions.

    10) Termination for Patent Action. This License shall terminate
    automatically and You may no longer exercise any of the rights granted
    to You by this License as of the date You commence an action,
    including a cross-claim or counterclaim, against Licensor or any
    licensee alleging that the Original Work infringes a patent. This
    termination provision shall not apply for an action alleging patent
    infringement by combinations of the Original Work with other software
    or hardware.

    11) Jurisdiction, Venue and Governing Law. Any action or suit relating
    to this License may be brought only in the courts of a jurisdiction
    wherein the Licensor resides or in which Licensor conducts its primary
    business, and under the laws of that jurisdiction excluding its
    conflict-of-law provisions. The application of the United Nations
    Convention on Contracts for the International Sale of Goods is
    expressly excluded. Any use of the Original Work outside the scope of
    this License or after its termination shall be subject to the
    requirements and penalties of the U.S. Copyright Act, 17 U.S.C. Â§ 101
    et seq., the equivalent laws of other countries, and international
    treaty. This section shall survive the termination of this License.

    12) Attorneys Fees. In any action to enforce the terms of this License
    or seeking damages relating thereto, the prevailing party shall be
    entitled to recover its costs and expenses, including, without
    limitation, reasonable attorneys' fees and costs incurred in
    connection with such action, including any appeal of such action. This
    section shall survive the termination of this License.

    13) Miscellaneous. This License represents the complete agreement
    concerning the subject matter hereof. If any provision of this License
    is held to be unenforceable, such provision shall be reformed only to
    the extent necessary to make it enforceable.

    14) Definition of "You" in This License. "You" throughout this
    License, whether in upper or lower case, means an individual or a
    legal entity exercising rights under, and complying with all of the
    terms of, this License. For legal entities, "You" includes any entity
    that controls, is controlled by, or is under common control with
    you. For purposes of this definition, "control" means (i) the power,
    direct or indirect, to cause the direction or management of such
    entity, whether by contract or otherwise, or (ii) ownership of fifty
    percent (50%) or more of the outstanding shares, or (iii) beneficial
    ownership of such entity.

    15) Right to Use. You may use the Original Work in all ways not
    otherwise restricted or conditioned by this License or by law, and
    Licensor promises not to interfere with or be responsible for such
    uses by You.

    This license is Copyright (C) 2003-2004 Lawrence E. Rosen. All rights
    reserved. Permission is hereby granted to copy and distribute this
    license without modification. This license may not be modified without
    the express written permission of its copyright owner.
